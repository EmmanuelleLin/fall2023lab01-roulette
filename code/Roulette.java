import java.util.Scanner;
class Roulette{
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        RouletteWheel myWheel = new RouletteWheel();
        int userMoney = 1000;
        boolean continueGameOrNot = true;
        
        while(continueGameOrNot == true){
            System.out.println("------------------------------------------------------");
            String betOrNot = "";
            System.out.println("Do you want to make a bet? y or n?");
            betOrNot = scan.next();
            if(betOrNot.equals("n")){
                continueGameOrNot = false;
                break;
            }
            System.out.println("Which number would you like to bet on? (0 to 36)");
            int userBetNum = scan.nextInt();
            while(userBetNum < 0 || userBetNum > 36){
                System.out.println("Please enter a number between 0 and 36.");
                userBetNum = scan.nextInt();
            }

            System.out.println("How much do you want to bet?");
            int userBetMoney = scan.nextInt();
            while(userBetMoney > userMoney || userBetMoney <= 0){
                System.out.println("Please enter your bet within your budget of " + userMoney);
                userBetMoney = scan.nextInt();
            }
            System.out.println("You bet on " + userBetNum + " for " + userBetMoney + "$");
            myWheel.spin();

            System.out.println("The wheel has been spun...");
            int rouletteNumSpun = myWheel.getValue();
            System.out.println("The number is ... " + rouletteNumSpun + "!!!");
            
            if(userBetNum == rouletteNumSpun){
                System.out.println("You won the roulette game!!! The money has been added to your money.");
                int winMoney = userBetMoney*35;
                userMoney += winMoney;
                System.out.println("You now have" + userMoney);
            }
            else{
                System.out.println("Your bet was " + userBetNum + ". Better luck next time!");
                userMoney -= userBetMoney;
                System.out.println("You have " + userMoney + " left.");
            }
            
            if(userMoney <= 0){
                System.out.println("You have no more money left. You can't play anymore :(");
                continueGameOrNot = false;
            }
        }
    }
}

